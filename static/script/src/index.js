new Vue(
{
    data :
    {
        isDiashier : false,
        isDiacgory : false,
        isDialog : false,
        dialogue :
        {
            diashier : "",
            diacgory : "",

            cashier  : "",
            category : "",
            product  : "",
            price    : 0
        },

        field :
        [
            { text : "No", value : 'id' },
            { text : "Cashier", value : 'cashier' },
            { text : "Category", value : 'category' },
            { text : "Product", value : 'product' },
            { text : "Price", value : 'price' }
        ],

        item :
        {
            cashiers   : [],
            categories : [],
            datas      : []
        }
    },

    methods :
    {
        close(type)
        {
            switch(type) {

                case "diashier" : this.isDiashier = false; break;
                case "diacgory" : this.isDiacgory = false; break;
                case "dialogue" : this.isDialog = false; break;
            }
        },

        save(type)
        {
            switch(type) {

                case "diashier" : this.item.cashiers.push(this.dialogue.diashier); this.dialogue.diashier = ""; this.close('diashier'); break;
                case "diacgory" : this.item.categories.push(this.dialogue.diacgory); this.dialogue.diacgory = ""; this.close('diacgory'); break;
                case "dialogue" : this.item.datas.push({ id : this.item.datas.length + 1, cashier : this.dialogue.cashier, category : this.dialogue.category, product : this.dialogue.product, price : this.dialogue.price }); this.dialogue.cashier = ""; this.dialogue.category = ""; this.dialogue.product = ""; this.dialogue.price = 0; this.close('dialogue'); break;
            }
        }
    },

    computed : {},

    watch : {},

    beforeCreate : function() {},

    created : function() {},

    beforeMount : function() {},

    mounted : function() {},

    beforeUpdate : function() {},

    updated : function() {},

    beforeDestroy : function() {},

    destroyed : function() {},

    el : document.body.querySelector('div'),

    vuetify : new Vuetify()
});