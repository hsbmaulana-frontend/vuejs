const Application = require('express'), Server = Application(), Router = Application.Router();

Server.use('/assets/', Application.static('static'));
Server.set('view engine', 'ejs');
Server.set('views', __dirname + '/public/');

require(__dirname + '/router.js')(Router);

Server.use('/', Router);

if(process.env.NODE_ENV === 'production') { Server.get('*', function(request, response) { response.sendFile(__dirname + '/public/error.html'); }); }

Server.listen(process.env.NODE_PORT || 8000, process.env.NODE_HOST || '127.0.0.1');