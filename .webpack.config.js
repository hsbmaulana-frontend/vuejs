module.exports =
{
    entry : require(__dirname + '/bundler.js'),

    module :
    {
        rules : [{ test : /\.js$/, exclude : /node_modules/, use : { loader : 'babel-loader', options : { presets : ['@babel/preset-env'] } } }]
    },

    resolve : { modules : [ __dirname + '/static/script/src/', 'node_modules/' ] },

    mode : process.env.NODE_ENV || 'production',

    output :
    {
        filename : process.env.NODE_ENV + '-' + 'bundle.[name].js',
        path : __dirname + '/static/script/dist/',
        publicPath : '/assets/'
    }
};